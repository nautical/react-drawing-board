import { IConfig } from 'umi-types';

const config: IConfig = {
  plugins: [
    ['umi-plugin-block-dev', {}],
    ['umi-plugin-react', {
      antd: true,
    }]
  ],
  theme: {
    "primary-color": "#5041bb",
  },
  disableCSSModules: true,
}

export default config;
