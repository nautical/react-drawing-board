import React from 'react';
export declare const DefaultConfig: {
    prefixCls: string;
};
declare const ConfigContext: React.Context<{
    prefixCls: string;
}>;
export default ConfigContext;
