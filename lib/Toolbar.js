"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("antd/lib/dropdown/style/css");

var _dropdown = _interopRequireDefault(require("antd/lib/dropdown"));

var _react = _interopRequireWildcard(require("react"));

var _reactSpring = require("react-spring");

var _reactIntl = require("react-intl");

var _Tool = _interopRequireDefault(require("./enums/Tool"));

var _SelectIcon = _interopRequireDefault(require("./svgs/SelectIcon"));

var _StrokeIcon = _interopRequireDefault(require("./svgs/StrokeIcon"));

var _ShapeIcon = _interopRequireDefault(require("./svgs/ShapeIcon"));

var _CameraIcon = _interopRequireDefault(require("./svgs/CameraIcon"));

var _DesktopIcon = _interopRequireDefault(require("./svgs/DesktopIcon"));

var _TextIcon = _interopRequireDefault(require("./svgs/TextIcon"));

var _ImageIcon = _interopRequireDefault(require("./svgs/ImageIcon"));

var _UndoIcon = _interopRequireDefault(require("./svgs/UndoIcon"));

var _RedoIcon = _interopRequireDefault(require("./svgs/RedoIcon"));

var _ClearIcon = _interopRequireDefault(require("./svgs/ClearIcon"));

var _ZoomIcon = _interopRequireDefault(require("./svgs/ZoomIcon"));

var _SaveIcon = _interopRequireDefault(require("./svgs/SaveIcon"));

var _EraserIcon = _interopRequireDefault(require("./svgs/EraserIcon"));

var _StrokeTool = require("./tools/stroke/StrokeTool");

var _ShapeTool = require("./tools/shape/ShapeTool");

var _CameraTool = require("./tools/camera/CameraTool");

var _DesktopTool = require("./tools/desktop/DesktopTool");

var _classnames = _interopRequireDefault(require("classnames"));

require("./Toolbar.css");

var _utils = require("./utils");

var _ConfigContext = _interopRequireDefault(require("./ConfigContext"));

var _EnableSketchPadContext = _interopRequireDefault(require("./contexts/EnableSketchPadContext"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var Toolbar = function Toolbar(props) {
  var _classNames;

  var currentTool = props.currentTool,
      setCurrentTool = props.setCurrentTool,
      currentToolOption = props.currentToolOption,
      setCurrentToolOption = props.setCurrentToolOption,
      selectImage = props.selectImage,
      undo = props.undo,
      redo = props.redo,
      clear = props.clear,
      save = props.save,
      toolbarPlacement = props.toolbarPlacement,
      zoomEnabled = props.zoomEnabled,
      eraserEnabled = props.eraserEnabled;
  var tools = [{
    label: 'umi.block.sketch.select',
    icon: _SelectIcon.default,
    type: _Tool.default.Select
  }, {
    label: 'umi.block.sketch.pencil',
    icon: _StrokeIcon.default,
    type: _Tool.default.Stroke,
    useDropdown: _StrokeTool.useStrokeDropdown
  }, {
    label: 'umi.block.sketch.shape',
    icon: _ShapeIcon.default,
    type: _Tool.default.Shape,
    useDropdown: _ShapeTool.useShapeDropdown
  }, {
    label: 'umi.block.sketch.text',
    icon: _TextIcon.default,
    type: _Tool.default.Text
  }, {
    label: 'umi.block.sketch.image',
    icon: _ImageIcon.default,
    type: _Tool.default.Image
  }, {
    label: 'umi.block.sketch.Camera',
    icon: _CameraIcon.default,
    type: _Tool.default.Camera,
    useDropdown: _CameraTool.useCameraDropdown
  }, {
    label: 'umi.block.sketch.Desktop',
    icon: _DesktopIcon.default,
    type: _Tool.default.Desktop,
    useDropdown: _DesktopTool.useDesktopDropdown
  }, {
    label: 'umi.block.sketch.undo',
    icon: _UndoIcon.default,
    type: _Tool.default.Undo,
    style: {
      marginLeft: 'auto'
    }
  }, {
    label: 'umi.block.sketch.redo',
    icon: _RedoIcon.default,
    type: _Tool.default.Redo
  }].concat(_toConsumableArray(eraserEnabled ? [{
    label: 'umi.block.sketch.eraser',
    icon: _EraserIcon.default,
    type: _Tool.default.Eraser
  }] : []), [{
    label: 'umi.block.sketch.clear',
    icon: _ClearIcon.default,
    type: _Tool.default.Clear,
    style: {
      marginRight: 'auto'
    }
  }], _toConsumableArray(!_utils.isMobileDevice && zoomEnabled ? [{
    label: '100%',
    labelThunk: function labelThunk(props) {
      return "".concat(~~(props.scale * 100), "%");
    },
    icon: _ZoomIcon.default,
    type: _Tool.default.Zoom
  }] : []), _toConsumableArray(!_utils.isMobileDevice ? [{
    label: 'umi.block.sketch.save',
    icon: _SaveIcon.default,
    type: _Tool.default.Save,
    style: {
      marginLeft: "300px"
    }
  }] : []));
  var refFileInput = (0, _react.useRef)(null);

  var _useIntl = (0, _reactIntl.useIntl)(),
      formatMessage = _useIntl.formatMessage;

  var _useContext = (0, _react.useContext)(_ConfigContext.default),
      prefixCls = _useContext.prefixCls;

  var enableSketchPadContext = (0, _react.useContext)(_EnableSketchPadContext.default);
  var toolbarPrefixCls = prefixCls + '-toolbar';

  var handleFileChange = function handleFileChange(e) {
    var file = e.target.files && e.target.files[0];

    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);

      reader.onloadend = function () {
        var base64data = reader.result;
        selectImage(base64data);
      };
    }
  };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: (0, _classnames.default)((_classNames = {}, _defineProperty(_classNames, "".concat(toolbarPrefixCls, "-container"), true), _defineProperty(_classNames, "".concat(toolbarPrefixCls, "-mobile-container"), _utils.isMobileDevice), _classNames))
  }, tools.map(function (tool) {
    var _classNames2;

    var borderTopStyle = 'none';

    if (_utils.isMobileDevice) {
      if (tool.type === _Tool.default.Stroke && currentToolOption.strokeColor) {
        borderTopStyle = "3px solid ".concat(currentToolOption.strokeColor);
      }

      if (tool.type === _Tool.default.Shape && currentToolOption.shapeBorderColor) {
        borderTopStyle = "3px solid ".concat(currentToolOption.shapeBorderColor);
      }
    }

    var iconAnimateProps = (0, _reactSpring.useSpring)(_objectSpread({
      left: _utils.isMobileDevice && currentTool !== tool.type ? -12 : 0,
      borderTop: borderTopStyle
    }, tool.style || {}));

    var menu = /*#__PURE__*/_react.default.createElement(_reactSpring.animated.div, {
      className: (0, _classnames.default)((_classNames2 = {}, _defineProperty(_classNames2, "".concat(toolbarPrefixCls, "-icon"), true), _defineProperty(_classNames2, "".concat(toolbarPrefixCls, "-activeIcon"), currentTool === tool.type && !_utils.isMobileDevice), _defineProperty(_classNames2, "".concat(toolbarPrefixCls, "-mobile-icon"), _utils.isMobileDevice), _classNames2)),
      style: iconAnimateProps,
      onClick: function onClick() {
        if (tool.type === _Tool.default.Image && refFileInput.current) {
          refFileInput.current.click();
        } else if (tool.type === _Tool.default.Undo) {
          undo();
        } else if (tool.type === _Tool.default.Redo) {
          redo();
        } else if (tool.type === _Tool.default.Clear) {
          clear();
        } else if (tool.type === _Tool.default.Zoom) {} else if (tool.type === _Tool.default.Save) {
          save();
        } else {
          setCurrentTool(tool.type);
        }
      },
      key: tool.label
    }, /*#__PURE__*/_react.default.createElement(tool.icon, null), !_utils.isMobileDevice ? /*#__PURE__*/_react.default.createElement("label", {
      className: "".concat(toolbarPrefixCls, "-iconLabel")
    }, tool.labelThunk ? tool.labelThunk(props) : formatMessage({
      id: tool.label
    })) : null);

    if (tool.useDropdown) {
      var overlay = tool.useDropdown(currentToolOption, setCurrentToolOption, setCurrentTool, prefixCls);
      return /*#__PURE__*/_react.default.createElement(_dropdown.default, {
        key: tool.label,
        overlay: overlay,
        placement: toolbarPlacement === 'top' || toolbarPlacement === 'left' ? 'bottomLeft' : 'bottomRight',
        trigger: [_utils.isMobileDevice ? 'click' : 'hover'],
        onVisibleChange: function onVisibleChange(visible) {
          enableSketchPadContext.setEnable(!visible);
        }
      }, menu);
    } else {
      return menu;
    }
  }), /*#__PURE__*/_react.default.createElement("input", {
    type: "file",
    style: {
      display: 'none'
    },
    accept: "image/jpeg, image/png",
    ref: refFileInput,
    onChange: handleFileChange
  }));
};

var _default = Toolbar;
exports.default = _default;