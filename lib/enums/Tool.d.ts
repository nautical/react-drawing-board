declare enum Tool {
    Select = "Select",
    Stroke = "Stroke",
    Shape = "Shape",
    Camera = "Camera",
    Desktop = "Desktop",
    Text = "Text",
    Image = "Image",
    Undo = "Undo",
    Redo = "Redo",
    Clear = "Clear",
    Fill = "Fill",
    Eraser = "Eraser",
    Zoom = "Zoom",
    Save = "Save",
    Update = "Update",
    LazyUpdate = "LazyUpdate",
    Remove = "Remove"
}
export declare enum ShapeType {
    Rectangle = "Rectangle",
    Oval = "Oval",
    Star = "Star"
}
export declare enum CameraType {
    Rectangle = "Rectangle",
    Oval = "Oval"
}
export declare enum DesktopType {
    Rectangle = "Rectangle",
    Oval = "Oval"
}
export declare const MAX_SCALE = 2;
export declare const MIN_SCALE = 0.1;
export interface Position {
    x: number;
    y: number;
    w: number;
    h: number;
}
export declare const strokeSize: number[];
export declare const strokeColor: string[];
export declare enum TextSize {
    Small = 12,
    Default = 20,
    Large = 28
}
export declare const defaultToolOption: {
    strokeSize: number;
    strokeColor: string;
    shapeType: ShapeType;
    cameraType: CameraType;
    desktopType: DesktopType;
    shapeBorderColor: string;
    fill: boolean;
    shapeBorderSize: number;
    textColor: string;
    textSize: TextSize;
    meta: string;
    defaultText: {
        id: string;
    };
};
export declare type ToolOption = {
    strokeSize: number;
    strokeColor: string;
    fill: boolean;
    meta: string;
    shapeType: ShapeType;
    shapeBorderColor: string;
    shapeBorderSize: number;
    cameraType: CameraType;
    desktopType: DesktopType;
    textColor: string;
    textSize: TextSize;
    defaultText: string | {
        id: string;
    };
};
export default Tool;
