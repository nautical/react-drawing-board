declare const _default: {
    'umi.block.sketch.select': string;
    'umi.block.sketch.pencil': string;
    'umi.block.sketch.shape': string;
    'umi.block.sketch.Camera': string;
    'umi.block.sketch.Desktop': string;
    'umi.block.sketch.text': string;
    'umi.block.sketch.image': string;
    'umi.block.sketch.undo': string;
    'umi.block.sketch.redo': string;
    'umi.block.sketch.clear': string;
    'umi.block.sketch.eraser': string;
    'umi.block.sketch.save': string;
    'umi.block.sketch.text.placeholder': string;
    'umi.block.sketch.text.size.small': string;
    'umi.block.sketch.text.size.default': string;
    'umi.block.sketch.text.size.large': string;
};
export default _default;
