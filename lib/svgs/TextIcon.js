"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TextIcon = function TextIcon(props) {
  return /*#__PURE__*/_react.default.createElement("svg", Object.assign({
    x: "0px",
    y: "0px",
    viewBox: "0 0 64 64"
  }, props), /*#__PURE__*/_react.default.createElement("polygon", {
    points: "51,14.3 13.5,14.3 13.5,17.3 30.7,17.3 30.7,53.3 33.7,53.3 33.7,17.3 51,17.3 "
  }));
};

var _default = TextIcon;
exports.default = _default;