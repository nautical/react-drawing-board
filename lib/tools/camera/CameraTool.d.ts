/// <reference types="react" />
import Tool, { ToolOption, CameraType } from '../../enums/Tool';
import './CameraTool.less';
export interface Position {
    x: number;
    y: number;
    w: number;
    h: number;
}
export declare type Camera = {
    type: CameraType;
    size: number;
    start: {
        x: number;
        y: number;
    };
    end: {
        x: number;
        y: number;
    } | null;
};
export declare const onCameraMouseDown: (x: number, y: number, toolOption: ToolOption) => Camera[];
export declare const onCameraMouseUp: (x: number, y: number, setCurrentTool: (tool: Tool) => void, handleCompleteOperation: (tool?: Tool, data?: Camera, pos?: Position) => void) => Camera[];
export declare const onCameraMouseMove: (x: number, y: number, context: CanvasRenderingContext2D) => void;
export declare const drawCameraBox: (rect: Camera, context: CanvasRenderingContext2D, hover: boolean) => any;
export declare const useCameraDropdown: (currentToolOption: ToolOption, setCurrentToolOption: (option: ToolOption) => void, setCurrentTool: (tool: Tool) => void, prefixCls: string) => JSX.Element;
