"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCameraDropdown = exports.drawCameraBox = exports.onCameraMouseMove = exports.onCameraMouseUp = exports.onCameraMouseDown = void 0;

var _Tool = _interopRequireWildcard(require("../../enums/Tool"));

var _react = _interopRequireDefault(require("react"));

require("./CameraTool.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var camera = null;

var onCameraMouseDown = function onCameraMouseDown(x, y, toolOption) {
  camera = {
    type: toolOption.cameraType,
    size: 1,
    start: {
      x: x,
      y: y
    },
    end: null
  };
  return [camera];
};

exports.onCameraMouseDown = onCameraMouseDown;

var draw = function draw(item, mouseX, mouseY, context, hover) {
  var startX = mouseX < item.start.x ? mouseX : item.start.x;
  var startY = mouseY < item.start.y ? mouseY : item.start.y;
  var widthX = Math.abs(item.start.x - mouseX);
  var widthY = Math.abs(item.start.y - mouseY);
  var cameraIconW = widthY < widthX ? widthY / 2 : widthX / 2;
  if (cameraIconW > 40) cameraIconW = 40; // keep it small .. some weird build issue

  if (item.type === _Tool.CameraType.Rectangle) {
    context.beginPath();
    context.lineWidth = item.size;
    context.strokeStyle = "#707070";
    context.lineWidth = 10;
    context.rect(startX, startY, widthX, widthY);
    context.stroke();
    context.closePath();
    context.fillStyle = "#282828";
    context.fill('evenodd');
    context.font = "".concat(cameraIconW, "px Arial");
    context.fillStyle = "#ffffff";
    context.fillText("📷", startX + widthX / 2 - cameraIconW / 2, startY + widthY / 2 + cameraIconW / 4);

    if (hover) {
      context.beginPath();
      context.strokeStyle = '#3AB1FE';
      context.lineWidth = item.size / 2;
      context.rect(startX - item.size / 2, startY - item.size / 2, widthX + item.size, widthY + item.size);
      context.stroke();
      context.closePath();
    }
  } else if (item.type === _Tool.CameraType.Oval) {
    var endX = mouseX >= item.start.x ? mouseX : item.start.x;
    var endY = mouseY >= item.start.y ? mouseY : item.start.y;
    var radiusX = (endX - startX) * 0.5;
    var radiusY = (endY - startY) * 0.5;
    var centerX = startX + radiusX;
    var centerY = startY + radiusY;
    context.beginPath();
    context.lineWidth = item.size;
    context.strokeStyle = "#707070";
    context.lineWidth = 10;

    if (typeof context.ellipse === 'function') {
      context.ellipse(centerX, centerY, radiusX, radiusY, 0, 0, 2 * Math.PI);
    } else {
      var xPos;
      var yPos;
      var i = 0;

      for (i; i < 2 * Math.PI; i += 0.01) {
        xPos = centerX - radiusY * Math.sin(i) * Math.sin(0) + radiusX * Math.cos(i) * Math.cos(0);
        yPos = centerY + radiusX * Math.cos(i) * Math.sin(0) + radiusY * Math.sin(i) * Math.cos(0);

        if (i === 0) {
          context.moveTo(xPos, yPos);
        } else {
          context.lineTo(xPos, yPos);
        }
      }
    }

    context.stroke();
    context.closePath();
    context.fillStyle = "#282828";
    context.fill('evenodd');
    context.font = "".concat(cameraIconW, "px Arial");
    context.fillStyle = "#ffffff";
    context.fillText("📷", startX + widthX / 2 - cameraIconW / 2, startY + widthY / 2 + cameraIconW / 4);

    if (hover) {
      context.beginPath();
      context.strokeStyle = '#3AB1FE';
      context.lineWidth = item.size / 2;

      if (typeof context.ellipse === 'function') {
        context.ellipse(centerX, centerY, radiusX + item.size / 2, radiusY + item.size / 2, 0, 0, 2 * Math.PI);
      } else {
        var _xPos;

        var _yPos;

        var _i = 0;

        for (_i; _i < 2 * Math.PI; _i += 0.01) {
          _xPos = centerX - (radiusY + item.size / 2) * Math.sin(_i) * Math.sin(0) + (radiusX + item.size / 2) * Math.cos(_i) * Math.cos(0);
          _yPos = centerY + (radiusX + item.size / 2) * Math.cos(_i) * Math.sin(0) + (radiusY + item.size / 2) * Math.sin(_i) * Math.cos(0);

          if (_i === 0) {
            context.moveTo(_xPos, _yPos);
          } else {
            context.lineTo(_xPos, _yPos);
          }
        }
      }

      context.stroke();
      context.closePath();
    }
  }
};

var onCameraMouseUp = function onCameraMouseUp(x, y, setCurrentTool, handleCompleteOperation) {
  if (!camera) return;
  var item = camera;
  camera = null;
  item.end = {
    x: x,
    y: y
  }; // avoid touch by mistake.

  if (Math.abs(item.start.x - item.end.x) + Math.abs(item.start.x - item.end.x) < 6) {
    return;
  }

  handleCompleteOperation(_Tool.default.Camera, item, {
    x: Math.min(item.start.x, item.end.x),
    y: Math.min(item.start.y, item.end.y),
    w: Math.abs(item.end.x - item.start.x),
    h: Math.abs(item.end.y - item.start.y)
  });
  setCurrentTool(_Tool.default.Select);
  return [item];
};

exports.onCameraMouseUp = onCameraMouseUp;

var onCameraMouseMove = function onCameraMouseMove(x, y, context) {
  if (!camera) return;
  draw(camera, x, y, context, false);
};

exports.onCameraMouseMove = onCameraMouseMove;

var drawCameraBox = function drawCameraBox(rect, context, hover) {
  if (!rect.end) return null;
  draw(rect, rect.end.x, rect.end.y, context, hover);
};

exports.drawCameraBox = drawCameraBox;

var useCameraDropdown = function useCameraDropdown(currentToolOption, setCurrentToolOption, setCurrentTool, prefixCls) {
  prefixCls = prefixCls += '-cameraTool';
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(prefixCls, "-strokeMenu")
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(prefixCls, "-camera")
  }, /*#__PURE__*/_react.default.createElement("div", {
    onClick: function onClick(evt) {
      evt.stopPropagation();
      setCurrentToolOption(_objectSpread(_objectSpread({}, currentToolOption), {}, {
        cameraType: _Tool.CameraType.Rectangle
      }));
      setCurrentTool(_Tool.default.Camera);
    },
    onTouchStart: function onTouchStart(evt) {
      evt.stopPropagation();
      setCurrentToolOption(_objectSpread(_objectSpread({}, currentToolOption), {}, {
        cameraType: _Tool.CameraType.Rectangle
      }));
      setCurrentTool(_Tool.default.Camera);
    },
    className: "".concat(prefixCls, "-cameraItem"),
    style: currentToolOption.cameraType === _Tool.CameraType.Rectangle ? {
      background: 'rgba(238, 238, 238, 1)'
    } : {}
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(prefixCls, "-rect"),
    style: currentToolOption.cameraType === _Tool.CameraType.Rectangle ? {
      borderColor: "red"
    } : {}
  })), /*#__PURE__*/_react.default.createElement("div", {
    onTouchStart: function onTouchStart(evt) {
      evt.stopPropagation();
      setCurrentToolOption(_objectSpread(_objectSpread({}, currentToolOption), {}, {
        cameraType: _Tool.CameraType.Oval
      }));
      setCurrentTool(_Tool.default.Camera);
    },
    onClick: function onClick(evt) {
      evt.stopPropagation();
      setCurrentToolOption(_objectSpread(_objectSpread({}, currentToolOption), {}, {
        cameraType: _Tool.CameraType.Oval
      }));
      setCurrentTool(_Tool.default.Camera);
    },
    className: "".concat(prefixCls, "-cameraItem"),
    style: currentToolOption.cameraType === _Tool.CameraType.Oval ? {
      background: 'rgba(238, 238, 238, 1)'
    } : {}
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "".concat(prefixCls, "-circle"),
    style: currentToolOption.cameraType === _Tool.CameraType.Oval ? {
      borderColor: "red"
    } : {}
  }))));
};

exports.useCameraDropdown = useCameraDropdown;