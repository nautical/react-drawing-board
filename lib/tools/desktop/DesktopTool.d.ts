/// <reference types="react" />
import Tool, { ToolOption, DesktopType } from '../../enums/Tool';
import './DesktopTool.less';
export interface Position {
    x: number;
    y: number;
    w: number;
    h: number;
}
export declare type Desktop = {
    type: DesktopType;
    size: number;
    start: {
        x: number;
        y: number;
    };
    end: {
        x: number;
        y: number;
    } | null;
};
export declare const onDesktopMouseDown: (x: number, y: number, toolOption: ToolOption) => Desktop[];
export declare const onDesktopMouseUp: (x: number, y: number, setCurrentTool: (tool: Tool) => void, handleCompleteOperation: (tool?: Tool, data?: Desktop, pos?: Position) => void) => Desktop[];
export declare const onDesktopMouseMove: (x: number, y: number, context: CanvasRenderingContext2D) => void;
export declare const drawDesktopBox: (rect: Desktop, context: CanvasRenderingContext2D, hover: boolean) => any;
export declare const useDesktopDropdown: (currentToolOption: ToolOption, setCurrentToolOption: (option: ToolOption) => void, setCurrentTool: (tool: Tool) => void, prefixCls: string) => JSX.Element;
