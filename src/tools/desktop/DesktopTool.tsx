import Tool, { ToolOption, DesktopType } from '../../enums/Tool';
import React from 'react';
import './DesktopTool.less';

export interface Position {
  x: number;
  y: number;
  w: number;
  h: number;
}

export type Desktop = {
  type: DesktopType;
  size: number;
  start: {
    x: number;
    y: number;
  };
  end: {
    x: number;
    y: number;
  } | null;
};

let desktop: Desktop | null = null;

export const onDesktopMouseDown = (x: number, y: number, toolOption: ToolOption) => {
  desktop = {
    type: toolOption.desktopType,
    size: 1,
    start: { x, y },
    end: null,
  };

  return [desktop];
};

const draw = (
  item: Desktop,
  mouseX: number,
  mouseY: number,
  context: CanvasRenderingContext2D,
  hover: boolean,
) => {
  const startX = mouseX < item.start.x ? mouseX : item.start.x;
  const startY = mouseY < item.start.y ? mouseY : item.start.y;
  const widthX = Math.abs(item.start.x - mouseX);
  const widthY = Math.abs(item.start.y - mouseY);
  let desktopIconW = widthY < widthX ? widthY/2 : widthX/2
  if(desktopIconW > 40) desktopIconW = 40 // keep it small .. some weird build issue

  if (item.type === DesktopType.Rectangle) {
    context.beginPath();
    context.lineWidth = item.size;
    context.strokeStyle = "#707070";
    context.lineWidth = 10
    context.rect(startX, startY, widthX, widthY);
    context.stroke();
    context.closePath();
    context.fillStyle = "#282828";
    context.fill('evenodd');

    context.font = `${desktopIconW}px Arial`;
    context.fillStyle = "#ffffff";
    context.fillText("🖥️", startX + widthX/2 - desktopIconW/2, startY + widthY/2 + desktopIconW/4)

    if (hover) {
      context.beginPath();
      context.strokeStyle = '#3AB1FE';
      context.lineWidth = item.size / 2;
      context.rect(
        startX - item.size / 2,
        startY - item.size / 2,
        widthX + item.size,
        widthY + item.size,
      );

      context.stroke();
      context.closePath();
    }
  } else if (item.type === DesktopType.Oval) {
    const endX = mouseX >= item.start.x ? mouseX : item.start.x;
    const endY = mouseY >= item.start.y ? mouseY : item.start.y;
    const radiusX = (endX - startX) * 0.5;
    const radiusY = (endY - startY) * 0.5;
    const centerX = startX + radiusX;
    const centerY = startY + radiusY;

    context.beginPath();
    context.lineWidth = item.size;
    context.strokeStyle = "#707070";
    context.lineWidth = 10

    if (typeof context.ellipse === 'function') {
      context.ellipse(centerX, centerY, radiusX, radiusY, 0, 0, 2 * Math.PI);
    } else {
      let xPos;
      let yPos;
      let i = 0;
      for (i; i < 2 * Math.PI; i += 0.01) {
        xPos = centerX - radiusY * Math.sin(i) * Math.sin(0) + radiusX * Math.cos(i) * Math.cos(0);
        yPos = centerY + radiusX * Math.cos(i) * Math.sin(0) + radiusY * Math.sin(i) * Math.cos(0);
        if (i === 0) {
          context.moveTo(xPos, yPos);
        } else {
          context.lineTo(xPos, yPos);
        }
      }
    }

    context.stroke();
    context.closePath();
    context.fillStyle = "#282828";
    context.fill('evenodd');
    
    context.font = `${desktopIconW}px Arial`;
    context.fillStyle = "#ffffff";
    context.fillText("🖥️", startX + widthX/2 - desktopIconW/2, startY + widthY/2 + desktopIconW/4)
    
    if (hover) {
      context.beginPath();
      context.strokeStyle = '#3AB1FE';
      context.lineWidth = item.size / 2;

      if (typeof context.ellipse === 'function') {
        context.ellipse(
          centerX,
          centerY,
          radiusX + item.size / 2,
          radiusY + item.size / 2,
          0,
          0,
          2 * Math.PI,
        );
      } else {
        let xPos;
        let yPos;
        let i = 0;
        for (i; i < 2 * Math.PI; i += 0.01) {
          xPos =
            centerX -
            (radiusY + item.size / 2) * Math.sin(i) * Math.sin(0) +
            (radiusX + item.size / 2) * Math.cos(i) * Math.cos(0);
          yPos =
            centerY +
            (radiusX + item.size / 2) * Math.cos(i) * Math.sin(0) +
            (radiusY + item.size / 2) * Math.sin(i) * Math.cos(0);
          if (i === 0) {
            context.moveTo(xPos, yPos);
          } else {
            context.lineTo(xPos, yPos);
          }
        }
      }

      context.stroke();
      context.closePath();
    }
  }
};

export const onDesktopMouseUp = (
  x: number,
  y: number,
  setCurrentTool: (tool: Tool) => void,
  handleCompleteOperation: (tool?: Tool, data?: Desktop, pos?: Position) => void,
) => {
  if (!desktop) return;

  const item = desktop;
  desktop = null;
  item.end = { x, y };

  // avoid touch by mistake.
  if (Math.abs(item.start.x - item.end.x) + Math.abs(item.start.x - item.end.x) < 6) {
    return;
  }

  handleCompleteOperation(Tool.Desktop, item, {
    x: Math.min(item.start.x, item.end.x),
    y: Math.min(item.start.y, item.end.y),
    w: Math.abs(item.end.x - item.start.x),
    h: Math.abs(item.end.y - item.start.y),
  });

  setCurrentTool(Tool.Select);

  return [item];
};

export const onDesktopMouseMove = (x: number, y: number, context: CanvasRenderingContext2D) => {
  if (!desktop) return;

  draw(desktop, x, y, context, false);
};

export const drawDesktopBox = (rect: Desktop, context: CanvasRenderingContext2D, hover: boolean) => {
  if (!rect.end) return null;

  draw(rect, rect.end.x, rect.end.y, context, hover);
};

export const useDesktopDropdown = (
  currentToolOption: ToolOption,
  setCurrentToolOption: (option: ToolOption) => void,
  setCurrentTool: (tool: Tool) => void,
  prefixCls: string,
) => {
  prefixCls = prefixCls += '-desktopTool';

  return (
    <div className={`${prefixCls}-strokeMenu`}>
      <div className={`${prefixCls}-desktop`}>
        <div
          onClick={(evt) => {
            evt.stopPropagation();
            setCurrentToolOption({ ...currentToolOption, desktopType: DesktopType.Rectangle });
            setCurrentTool(Tool.Desktop);
          }}
          onTouchStart={(evt) => {
            evt.stopPropagation();
            setCurrentToolOption({ ...currentToolOption, desktopType: DesktopType.Rectangle });
            setCurrentTool(Tool.Desktop);
          }}
          className={`${prefixCls}-desktopItem`}
          style={
            currentToolOption.desktopType === DesktopType.Rectangle
              ? { background: 'rgba(238, 238, 238, 1)' }
              : {}
          }
        >
          <div
            className={`${prefixCls}-rect`}
            style={
              currentToolOption.desktopType === DesktopType.Rectangle
                ? { borderColor: "red" }
                : {}
            }
          />
        </div>

        <div
          onTouchStart={(evt) => {
            evt.stopPropagation();
            setCurrentToolOption({ ...currentToolOption, desktopType: DesktopType.Oval });
            setCurrentTool(Tool.Desktop);
          }}
          onClick={(evt) => {
            evt.stopPropagation();
            setCurrentToolOption({ ...currentToolOption, desktopType: DesktopType.Oval });
            setCurrentTool(Tool.Desktop);
          }}
          className={`${prefixCls}-desktopItem`}
          style={
            currentToolOption.desktopType === DesktopType.Oval
              ? { background: 'rgba(238, 238, 238, 1)' }
              : {}
          }
        >
          <div
            className={`${prefixCls}-circle`}
            style={
              currentToolOption.desktopType === DesktopType.Oval
                ? { borderColor: "red" }
                : {}
            }
          />
        </div>
      </div>
    </div>
  );
};
